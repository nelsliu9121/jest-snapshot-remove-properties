![npm](https://img.shields.io/npm/v/jest-snapshot-remove-properties.svg)
![Bitbucket issues](https://img.shields.io/bitbucket/issues/nelsliu9121/jest-snapshot-remove-properties.svg)
![npm bundle size (minified + gzip)](https://img.shields.io/bundlephobia/minzip/jest-snapshot-remove-properties.svg)
[![buddy pipeline](https://app.buddy.works/nelsliu9121/jest-snapshot-remove-properties/pipelines/pipeline/150033/badge.svg?token=a8dac0c1652ebc7cccb44dc2d2acb39c8e4935b9b343b69b772ef1b128df2da0 "buddy pipeline")](https://app.buddy.works/nelsliu9121/jest-snapshot-remove-properties/pipelines/pipeline/150033)

# Remove properties from snapshot

Use this serializer to remove any unwanted properties from snapshot, keeping it sparkling clean :sparkles:

## Install

Add the package as a dev dependency

```bash
# With npm
npm install --save-dev jest-snapshot-remove-properties

# With yarn
yarn add --dev jest-snapshot-remove-properties
```

[Register serializer with Jest](jest-snapshot-remove-properties) in `setupTestFrameworkScript.js`:

```js
import snapshotRemoveProperties from "jest-snapshot-remove-properties";

expect.addSnapshotSerializer(snapshotRemoveProperties(["bolluck-attr"]));
```

## Vanilla JS Example

```js
test('should remove data attributes', () => {
  expect(<Component bolluck-attr="bye" id="keep-me">Children</Component>).toMatchSnapshot();
});
```

Will output:

```js
exports[`should remove data attributes`] = `
<div
  id="keep-me"
>
  Children
</div>
`;
```
